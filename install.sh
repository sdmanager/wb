#!/bin/bash
SDMOPCDIR="/etc/sdmOpcServer"
TEMP=$(mktemp -d)
NEEDLINK=0
#REQUIRED_PKG="git"
#PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $REQUIRED_PKG|grep "ok installed")
#echo Checking for $REQUIRED_PKG: $PKG_OK
#if [ "" = "$PKG_OK" ]; then
#  echo "No $REQUIRED_PKG. Setting up $REQUIRED_PKG."
#  apt-get --yes install $REQUIRED_PKG
#fi

#if $(git clone -q https://gitlab.com/sdmanager/wb.git $TEMP) >0
#then
#echo "Cloning GIT OK"
#else
#echo "Error cloning GIT" >&2
#rm -f -r -d $TEMP
#exit
#fi
curl -O https://gitlab.com/sdmanager/wb/-/archive/master/wb-master.tar.gz
tar -xvf wb-master.tar.gz -C $TEMP
rm -f -r -d wb-master.tar.gz
TEMP=$TEMP/"wb-master"
if ! [ -d $SDMOPCDIR ]; then

  echo "Create $SDMOPCDIR directory"
  NEEDLINK=1
  if $(mkdir -p $SDMOPCDIR); then
    cp -aR $TEMP"/sdmOpcServer/." $SDMOPCDIR
    cp -u $TEMP"/opcServer.service" "/etc/systemd/system/opcServer.service"
    systemctl enable opcServer
    chmod +x $TEMP"/wbSnReader"
    SH=$($TEMP"/wbSnReader")
    if [ "" = "$SH" ]; then
      echo "error SH read from MQTT" >&2
      rm -f -r -d $TEMP
      exit
    else
      echo "SH wirenboard $SH"
      mv $SDMOPCDIR"/configs/_replace_with_identifier_.globalid" $SDMOPCDIR"/configs/wb-$SH.globalid"
    fi
  else
    echo "Create $SDMOPCDIR directory error" >&2
    rm -f -r -d $TEMP
    exit
  fi

else
  systemctl stop opcServer
  cp -u $TEMP"/sdmOpcServer/opcServer" $SDMOPCDIR"/opcServer"

  cp -u -R $TEMP"/sdmOpcServer/configs/transforms/." $SDMOPCDIR"/configs/transforms"

fi
chmod +x $SDMOPCDIR"/opcServer"
systemctl start opcServer
rm -f -r -d $TEMP
sleep 5