package main

import (
	"encoding/json"
	"fmt"
	"github.com/jessevdk/go-flags"
	log "github.com/sirupsen/logrus"
	"gitlab.com/sdmanager/ops/internal/crypto/cryptoAES"
	"net"
	"os"
	"strings"
)

var c struct {
	DevicePassword *string `short:"p" long:"password" description:"device password"`
	SetLink        link
}

var parser = flags.NewParser(&c, flags.Default)

func init() {
	_, err := parser.AddCommand("link",
		"set link",
		"set link to device",
		&c.SetLink)
	if err != nil {
		log.Errorf("error append command: %s", err)
		os.Exit(1)
	}
}
func main() {

	_, err := parser.Parse()
	if err != nil {
		if err != flags.ErrHelp {
			switch flagsErr := err.(type) {
			case flags.ErrorType:
				if flagsErr == flags.ErrHelp {
					os.Exit(0)
				}
				os.Exit(1)
			default:
				os.Exit(1)
			}
			return
		}
	}
}

type link struct {
}

func (l *link) Execute(args []string) error {

	var t struct {
		Link string `json:"link"`
	}

	t.Link = strings.Join(args, "")
	b, err := json.Marshal(t)
	if err != nil {
		return err
	}
	if c.DevicePassword == nil {
		return fmt.Errorf("device password required")
	}
	pc, err := net.ListenPacket("udp4", ":50190")
	if err != nil {
		return err
	}
	defer func(pc net.PacketConn) {
		_ = pc.Close()
	}(pc)

	addr, err := net.ResolveUDPAddr("udp4", "255.255.255.255:50150")
	if err != nil {
		return err
	}
	data := cryptoAES.KeyEncrypt(*c.DevicePassword, string(b[:]))
	_, err = pc.WriteTo([]byte(data), addr)
	if err != nil {
		return err
	}

	fmt.Println("UDP link sent successfully")
	return nil
}
