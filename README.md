
# OPC server

Сервер проекта SDM для wirenboard 


## Installation

Скачать скрипт на устройство

```bash
   curl -O https://gitlab.com/sdmanager/wb/-/raw/master/install.sh
```

Установить права на выполнение скрипта

```bash
  chmod +x install.sh
```

Выполнить скрипт установки

```bash
   ./install.sh
```

После окончиния установки скрипт запросит URL до link файла. Взять его можно на странице добавления нового устройства viewer
